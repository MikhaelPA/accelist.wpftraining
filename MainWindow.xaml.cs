﻿using Accelist.WpfTraining.Services;
using Accelist.WpfTraining.Validators;
using Microsoft.Win32;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Accelist.WpfTraining
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly ApiAccessService _ApiAccessMan;
        public string UserID { get; set; }
        public List<ContentModel> ContentList { get; set; }
        public MainWindow(ApiAccessService apiAccessService)
        {
            _ApiAccessMan = apiAccessService;
            InitializeComponent();
            ContentList = new List<ContentModel>();
        }

        private async Task LoginAsync()
        {
            var name = TextBoxName.Text;
            var uri = @$"http://localhost:5000/api/User?name={name}";

            if (string.IsNullOrEmpty(name))
            {
                var response = await _ApiAccessMan.GetDataAsync<List<UserIdRequestModel>>(uri);
                DataGridUpload.ItemsSource = response;

                MessageBox.Show("Login Failed, Please input your name.");
                return;
            }
            else
            {
                var response = await _ApiAccessMan.GetDataAsync(uri);
                UserID = response;

                if (string.IsNullOrEmpty(UserID))
                {
                    MessageBox.Show("Login Failed, Please check your log.");
                }
            }

        }


        private async void BtnEnterName_Click(object sender, RoutedEventArgs e)
        {
            await LoginAsync();
            LabelUserID.Content = $"Your UserID: {UserID}";
        }

        private void BtnUpload_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog
            {
                DefaultExt = ".txt", // Required file extension 
                Filter = "Text documents (.txt)|*.txt" // Optional file extensions
            };

            if (fileDialog.ShowDialog() == true)
            {
                TextBoxFileName.Text = fileDialog.FileName;
                try
                {
                    using var sr = new StreamReader(fileDialog.FileName);


                    // Read your txt file here
                    var contentFile = sr.ReadToEnd();
                    var lines = contentFile.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var line in lines)
                    {
                        var objects = line.Split('|');
                        var content = new ContentModel
                        {
                            Subject = objects[0],
                            Content = objects[1]
                        };
                        ContentList.Add(content);
                    }


                    DataGridUpload.ItemsSource = ContentList;
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Something wrong in uploading file");
                    MessageBox.Show("Upload failed, Please check your log.");
                }
            }
        }

        private void BtnValidate_Click(object sender, RoutedEventArgs e)
        {
            var validator = new ContentValidator();
            var errors = new List<ValidationContentModel>();
            foreach (var line in ContentList)
            {
                var result = validator.Validate(line);
                if (result.IsValid == false)
                {
                    var error = new ValidationContentModel
                    {
                        Subject = line.Subject,
                        Content = line.Content,
                        ErrorMessage = result.ToString("\n")
                    };
                    errors.Add(error);
                }
            }
            DataGridValidate.ItemsSource = errors;
        }
    }
}
