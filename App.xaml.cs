﻿using Accelist.WpfTraining.Services;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Accelist.WpfTraining
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        
        public App()
        {
            // Configure Serilog - using Serilog
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .WriteTo.RollingFile(@"C:\AspNetCoreLog\WPF\wpf-training-log-{Date}.txt")
                .CreateLogger();

            // Configure Service Dependecy Injection - using Microsoft.Extensions.DependencyInjection;
            ServiceCollection services = new ServiceCollection();
            ConfigureServices(services);
            serviceProvider = services.BuildServiceProvider();

        }

        private readonly ServiceProvider serviceProvider;
        private void ConfigureServices(ServiceCollection services)
        {
            // Add others
            services.AddHttpClient();

            // Add singleton window
            services.AddSingleton<MainWindow>();

            // Add transient service
            services.AddTransient<ApiAccessService>();
        }

        /// <summary>
        /// Create Method that will be called on startup. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnStartup(object sender, StartupEventArgs e)
        {
            var mainWindow = serviceProvider.GetService<MainWindow>();
            mainWindow.Show();
        }

    }
}
