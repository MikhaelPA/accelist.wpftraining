﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Accelist.WpfTraining.Validators
{
    public class ContentModel
    {
        public string Subject { get; set; }
        public string Content { get; set; }
    }

    public class ValidationContentModel : ContentModel
    {
        public string ErrorMessage { get; set; }
    }

    public class ContentValidator : AbstractValidator<ContentModel>
    {
        public List<ContentModel> ExcelData { get; set; }

        public ContentValidator()
        {
            RuleFor(Q => Q.Subject)
                .Cascade(CascadeMode.Stop)
                .NotEmpty().WithMessage("Subject must not be empty.")
                .Length(1,25).WithMessage("Subject must be between 1-25 characters.");

            RuleFor(Q => Q.Content)
                .Cascade(CascadeMode.Stop)
                .NotEmpty().WithMessage("Content must not be empty.")
                .Length(1, 500).WithMessage("Content must be between 1-500 characters.");
        }

    }
}
