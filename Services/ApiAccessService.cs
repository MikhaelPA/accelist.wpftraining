﻿using Microsoft.Extensions.Logging;
//using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Accelist.WpfTraining.Services
{
    public class ApiAccessService
    {
        private readonly IHttpClientFactory _ClientFactory;
        private readonly HttpClient _Client;
        /// <summary>
        /// Constructor
        /// Installing Dependency injection here
        /// </summary>
        public ApiAccessService(IHttpClientFactory clientFactory)
        {
            _ClientFactory = clientFactory;
            _Client = _ClientFactory.CreateClient();
        }

        public async Task<string> GetDataAsync(string uri)
        {

            try
            {
                var response = await _Client.GetAsync(uri);
                response.EnsureSuccessStatusCode();

                var contentStr = await response.Content.ReadAsStringAsync();

                return contentStr;
            }
            catch (Exception e)
            {
                Log.Error(e, "Error Occured in {className} with param:{param}", "ApiAccessService", uri);
                return default;
            }
        }

        public async Task<T> GetDataAsync<T>(string uri)
        {
            try
            {
                var response = await _Client.GetAsync(uri);

                response.EnsureSuccessStatusCode();

                // read json stream
                using var contentStr = await response.Content.ReadAsStreamAsync();

                // desserialize json into object
                var responseData = await System.Text.Json.JsonSerializer.DeserializeAsync<T>(contentStr);

                return responseData;//return contentStr;
            }
            catch (Exception e)
            {
                Log.Error(e, "Error Occured in {className} with param:{param}", "ApiAccessService", uri);
                return default;
            }
        }

        public async Task<T> PostDataAsync<T, C>(string uri, C bodyRequest)
        {
            try
            {
                var response = await _Client.PostAsJsonAsync(uri, bodyRequest);
                response.EnsureSuccessStatusCode();

                // read json string
                using var contentStr = await response.Content.ReadAsStreamAsync();

                // desserialize json into object
                var responseData = await JsonSerializer.DeserializeAsync<T>(contentStr);
                return responseData;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Log.Error(e, "Error Occured in {className} with param:{param}", "ApiAccessService", uri);
                return default;
            }
        }
    }
}
